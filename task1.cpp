#include <pthread.h>
#include <cmath>
#include <ctime>
#include <iostream>
#include <random>

#define NUM_THREADS 4
typedef std::mt19937 MyRNG; 
MyRNG generator;
int reduction_array[NUM_THREADS];
const long TOTALITERATIONS = 100000000;
//function predeclarations
void *montecarlo(void *threadid);

int main(int argc, char *argv[]) {
  _Uint32t seednumber = time(0);
  generator.seed(seednumber);
  pthread_t threads[NUM_THREADS];
  int rc;
  long in = 0;
  float _pi = 0;
  clock_t start_time;
  start_time=clock();
  //creating the threads
  for(int i = 0; i < NUM_THREADS; i++) {
    rc = pthread_create(&threads[i], NULL, montecarlo, (void*) i);
    if(rc) {
      printf("ERROR; return code from ptrhead:create() is %d\n", rc);
      exit(-1);
    }
  }
  //waiting for threads to finish
  for( int i = 0; i < NUM_THREADS; i++) {
     rc = pthread_join(threads[i], NULL);
     if(rc) {
      printf("ERROR; return code from ptrhead_join%d\n", rc);
      exit(-1);
     }
  }
  //adds the array elements together
  for(int i = 0; i < NUM_THREADS; i++) {
    in += reduction_array[i];
  }
  //calculating Pi
  _pi = ((float)in / TOTALITERATIONS)*4;
  clock_t total_time = (clock()-start_time)/CLOCKS_PER_SEC;
  printf("PI = %f",_pi);
  printf("\tTime = %d\n",total_time);
  printf("Press ENTER to exit");
  std::getchar();
  return 0;
}
//Function handling the montecarlo calculations
void *montecarlo(void *threadid) {
  int thread = (int)threadid;
  double number1, number2;
  for(long i = 0; i < TOTALITERATIONS/4; i++){     
      number1 = ((double) generator() / generator.max());
      number2 = ((double) generator() / generator.max());
    if(((number1*number1) + (number2*number2)) <= 1) {
      reduction_array[thread]++;
        } 
    
  } 
  pthread_exit(NULL); //close thread
  return 0;  
}

